# encoding: utf-8
require_relative '../test_helper'

module Payeleven
  class CustomerTest < Test::Unit::TestCase
=begin
    test 'should create customer' do
      customer = test_customer({})
      assert_raises (Payleven::PaylevenError) {
        response = customer.create(get_data_create()) 
        puts ">>>> #{response}"
      }
    end
    
    test 'should get status' do
      customer = test_customer({})
      response = customer.get_status(get_data_get_status()) 
      puts ">>>> #{response}"
    end

    should 'should update customer' do
      customer = test_customer({})
      params = get_data_create()
      params_update = {'payleven_id' => '136293', 'last_name' => 'last_name update'}
      params.merge!(params_update)
      response = customer.update(params) 
      assert_not_nil response
      assert_equal response["message"], "Successful."
    end

    should 'should update bank account' do
      customer = test_customer({})
      response = customer.update_bank_account(get_data_update_bank_account())
      assert_not_nil response
      assert_equal response["message"], "OK"
    end


    should 'should get deposits' do
      customer = test_customer({})
      response = customer.get_deposits(get_data_get_deposits())
      puts response
      assert_not_nil response
      assert_equal response["message"], "Successful."
    end
=end
  private
  
    def get_data_get_deposits
      data = {
        'customer_id' => '136165',
        'from' => '2015-08-01',
        'to' => '2015-08-10',
        'timestamp' => Payleven::BaseService.get_timestamp }
      data
    end
    
    def get_data_get_status
      data = {
        'payleven_id' => '136165',
        'timestamp' => Payleven::BaseService.get_timestamp }
      data
    end
    
    def get_data_update_bank_account
      data = {
        'payleven_id' => '136293',
        'merchant_reference' => 'testedds_3',
        'bank_id' => '341',
        'account_holder' => 'JOHN D CARMACK',
        'branch' => '8098',
        'document_type' => 'individual',
        'document_number' => '25319821409',
        'account_type' => 'checking',
        'account_number' => '00172',
        'account_number_digit' => '3',
        'timestamp' => Payleven::BaseService.get_timestamp }
      data
    end
    
    def get_data_create
      data = {
          'first_name' => 'FirstName',
          'last_name' => 'LastName',
          'gender' => 'M',
          'address_street' => 'Rua 123456',
          'address_district' => 'Bairro 123456',
          'address_postalcode' => '12345000',
          'address_complement' => 'Complemento 123',
          'address_city' => 'Belo Horizonte',
          'address_state' => 'MG',
          'address_country' => 'BR',
          'birth_date' => '01/01/1970',
          'telephone' => '(11) 1234-1234',
          'cellphone' => '(11) 4321-4321',
=begin
          'document' => '12.123.23-X',
          'company_name' => 'Empresa 123 Ltda',
          'company_document' => '123456/0001-21',
          'trade_name' => 'Empresa 123',
          'company_telephone' => '(11) 4567-4567',
          'company_other_telephone' => '(11) 6789-6789',
          'company_fax' => '(11) 7890-7890',
          'company_address_street' => 'Rua Empresa 123456',
          'company_address_complement' => 'Complemento Empresa',
          'company_address_district' => 'Distrito 123',
          'company_address_city' => 'Belo Horizonte',
          'company_address_state' => 'MG',
          'company_address_country' => 'BR',
          'company_address_postalcode' => '45678000',
          'device' => 'Galaxy S IV',
          'device_type' => 'Android',
          'mgm' => 'example@example.org',
          'device_internet' => 'sim',
          'working_area' => '4789',
          'transactions_month' => '100',
          'values' => '1000',
=end
          'password' => 'hash123456',
          'cpf' => '23219602568',
          'email' => 'testedds_6@doisdoissete.com',
          'merchant_reference' => '123456789',
          'bank_id' => '000',
          'account_holder' => 'Teste dds6',
          'branch' => '1212',
          'branch_digit' => '',
          'document_type' => 'individual',
          'document_number' => '12473220255',
          'account_type' => 'checking',
          'account_number' => '00170',
          'account_number_digit' => '6',
          'timestamp' => Payleven::BaseService.get_timestamp }
          
      data
    end
  end
end