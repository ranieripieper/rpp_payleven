# encoding: utf-8
require_relative '../test_helper'

module Payeleven
  class TransactionTest < Test::Unit::TestCase

=begin
    should 'should get transactions' do
      transaction = test_transaction({})
      response = transaction.get_transactions(get_data_get_transactions()) 
      puts response
    end

    should 'should create token' do
      transaction = test_transaction({})
      authorize = test_authorize({})
      access_token = authorize.authorize(Payleven.username, Payleven.password)

      response = transaction.create_token('4012001037141112', 'MICHAEL ABRASH', 'VISA', '123', '05/18', access_token["access_token"]) 
      puts response
    end

    should 'should process with token' do
      transaction = test_transaction({})
      authorize = test_authorize({})
      access_token = authorize.authorize(Payleven.username, Payleven.password)

      response = transaction.process_with_token('109.0', '136165', '123', 'ABCDEF123456', 'VISA', '3wc5xrwf4co40884cwo4co8ocwo0gw04g0owkc80o4sw884s0g', access_token["access_token"]) 
      puts response
    end
    
  
    should 'should authorize with token and capture with tid' do
      transaction = test_transaction({})
      authorize = test_authorize({})
      access_token = authorize.authorize(Payleven.username, Payleven.password)

      response = transaction.authorize_with_token('119.0', '136165', '123', 'ABCDEF123456', 'VISA', '3wc5xrwf4co40884cwo4co8ocwo0gw04g0owkc80o4sw884s0g', access_token["access_token"]) 
      puts response
      
      capture_resp = transaction.capture_with_tid('123', '119.0', response["TransactionID"], access_token["access_token"]) 
      puts capture_resp
    end

  
    should 'should send receipt' do
      transaction = test_transaction({})
      authorize = test_authorize({})
      access_token = authorize.authorize(Payleven.username, Payleven.password)

      response = transaction.send_receipt('2519751', 'rani.pieper@doisdoissete.com','5511976444919', access_token["access_token"]) 
      puts response
    end


  
    should 'should refund' do
      transaction = test_transaction({})
      response = transaction.refund('2519751') 
      puts response
    end
=end

  private
  
    def get_data_get_transactions
      data = {
        'customer_id' => '136165',
        'from' => '2015-08-01',
        'to' => '2015-08-11',
        'timestamp' => Payleven::BaseService.get_timestamp }
      data
    end
    
  end
end