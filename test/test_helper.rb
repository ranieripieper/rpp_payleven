require_relative '../lib/payleven'
require_relative '../lib/payleven/errors'
require 'test/unit'
require 'shoulda'

def test_authorize(params = {})
  return Payleven::Authorize.new()
end
  
def test_base_service(params = {})
  return Payleven::BaseService.new()
end

def test_customer(params = {})
  return Payleven::Customer.new()
end

def test_transaction(params = {})
  return Payleven::Transaction.new()
end

class Test::Unit::TestCase

  def setup
    #PagarMe.api_key="d9d9a83bf099df0396a8177593c52628f20ad86b"
  end

  def teardown
    #PagarMe.api_key=nil
  end
end