require "payleven/version"
require 'digest/sha1'
require_relative 'payleven/base_service'
require_relative 'payleven/errors'
require_relative 'payleven/util'
require_relative 'payleven/authorize'
require_relative 'payleven/request'
require_relative 'payleven/customer'
require_relative 'payleven/transaction'

module Payleven

  @@access_key = 'd9d9a83bf099df0396a8177593c52628f20ad86b'
  @@secret_key = 'b8b5be6b13ff089e02e1102e33399386a1c46700bcee261246d9dfa6ac59d43e95bb90b0ea21b9fb42a64115d61a602d14119c91cf840f36df7194a299a056a2'
  @@username = 'payleven-test'
  @@password = 'd9d9a83bf099df0396a8177593c52628f20ad86b'
  @@live = false

  @@ENDPOINT_GATEWAY = 'http://staging-api.payleven.com.br'
  @@ENDPOINT_MINHACONTA = 'http://minhaconta-staging.payleven.com.br'
  @@ENDPOINT_PAINEL = 'http://painel-staging.payleven.com.br'
  
  def self.endpoint_gateway
    @@ENDPOINT_GATEWAY
  end
  
  def self.endpoint_minha_conta
    @@ENDPOINT_MINHACONTA
  end
  
  def self.endpoint_painel
    @@ENDPOINT_PAINEL
  end
  
  def self.access_key=(access_key)
    @@access_key = access_key
  end

  def self.access_key
    @@access_key
  end
  
  def self.secret_key=(secret_key)
    @@secret_key = secret_key
  end

  def self.secret_key
    @@secret_key
  end

  def self.username=(username)
    @@username = username
  end

  def self.username
    @@username
  end
  
  def self.password=(password)
    @@password = password
  end

  def self.password
    @@password
  end

  def self.full_api_url(host, relative_path)
    "#{host}#{relative_path}"
  end

end
