module Payleven
  class PaylevenError < StandardError
    attr_accessor :http_status
    attr_accessor :message
    attr_accessor :parameter_name
    attr_accessor :type
    attr_accessor :url
    attr_accessor :errors

    def initialize(http_status = nil, message = "", parameter_name = "", type = "", url = "") 
      self.http_status = http_status
      self.message = message
      self.type = type
      self.parameter_name = parameter_name
      self.errors = []
    end

    def to_s
      "#{self.class.to_s} - #{self.http_status} - #{message}"
    end

  end

  class RequestError < PaylevenError
  end

  class ConnectionError < PaylevenError
  end

  class ResponseError < PaylevenError
  end
end