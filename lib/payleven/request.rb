require 'uri'
require 'rest_client'
require 'multi_json'
require "rack"

require File.join(File.dirname(__FILE__), '.', 'util')
require File.join(File.dirname(__FILE__), '.', 'errors')

module Payleven
  class Request
    attr_accessor :api_host, :path, :method, :parameters, :headers, :query

    def initialize(path, method, api_host = Payleven.endpoint_gateway)
      self.path = path
      self.method = method
      self.parameters = {}
      self.query = {}
      self.headers = {}
      self.api_host = api_host
    end

    def run
      raise PaylevenError, "You need to configure a API key before performing requests." unless Payleven.username
      
      url = Payleven.full_api_url(self.api_host, self.path)
      
      if not query.nil? and not query.empty? then
        url += "?#{CGI.unescape(Rack::Utils.build_nested_query(query))}"
      end

      headers.merge!({'Content-Type' => 'application/json; charset=utf8',
            'Accept' => 'application/json',
            'User-Agent' => 'pagarme-ruby/1.0'})
            
      puts "url : #{url}"
      puts "parametros: #{parameters}"
      puts "headers #{headers}"
      
      begin
        response = RestClient::Request.execute({
          :method => self.method,
          :url => url,
          :payload => parameters,
          :open_timeout => 30,
          :timeout => 90,
          :headers => headers
        })
      rescue RestClient::ExceptionWithResponse => e
        puts "Error = #{e}"
        raise PaylevenError.new(e.http_code, e.http_body)
      end
      puts "Result = #{response.body}"
      if not response.body.nil? and not response.body.empty?
        MultiJson.decode response.body
      else
        response.body
      end
      
    end
  end
end