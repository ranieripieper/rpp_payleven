# encoding: utf-8
require 'openssl'
require 'base64'
require File.join(File.dirname(__FILE__), '..', 'payleven')

module Payleven
  class Customer < BaseService
    
    URL_CREATE_CUSTOMER = '/api/customer/create/'
    URL_UPDATE_CUSTOMER = '/api/customer/update/'
    URL_UPDATE_BANK_ACCOUNT = '/api/customer/bank/update/'
    URL_GET_DEPOSITS = '/partner/deposits'
    URL_STATUS = '/api/customer/status/'
    
    def create(data)
      request = Payleven::Request.new(URL_CREATE_CUSTOMER, 'POST', Payleven.endpoint_painel)
      params = {
                  "entity" => data, 
                  "access" => Payleven.access_key, 
                  "token" => signature_token(data, Payleven.secret_key)}
      puts request.parameters
      request.parameters.merge!(params)
      
      response = request.run
      response
    end
    
    def update(data)
      request = Payleven::Request.new(URL_UPDATE_CUSTOMER, 'POST', Payleven.endpoint_painel)
      params = {
                  "entity" => data, 
                  "access" => Payleven.access_key, 
                  "token" => signature_token(data, Payleven.secret_key)}
                  
      request.parameters.merge!(params)
      
      response = request.run
      response
    end
    
    def update_bank_account(data) 
      request = Payleven::Request.new(URL_UPDATE_BANK_ACCOUNT, 'POST', Payleven.endpoint_painel)
      params = {
                  "entity" => data, 
                  "access" => Payleven.access_key, 
                  "token" => signature_token(data, Payleven.secret_key)}

      request.parameters.merge!(params)
      
      response = request.run
      response
    end
    
    def get_deposits(data) 
      request = Payleven::Request.new(URL_GET_DEPOSITS, 'GET', Payleven.endpoint_minha_conta)
      params = {
                  :entity => data, 
                  :access => Payleven.access_key, 
                  :token => signature_token(data, Payleven.secret_key)}

      request.query.merge!(params)

      response = request.run
      response
    end
    
    def get_status(data) 
      request = Payleven::Request.new(URL_STATUS, 'GET', Payleven.endpoint_painel)
      params = {
                  :entity => data, 
                  :access => Payleven.access_key, 
                  :token => signature_token(data, Payleven.secret_key)}

      request.query.merge!(params)

      response = request.run
      response
    end
  end
end