# encoding: utf-8
require 'openssl'
require 'base64'
require File.join(File.dirname(__FILE__), '..', 'payleven')

module Payleven
  class Authorize < Payleven::BaseService
    
    PATH_AUTHORIZE = '/partner/authorize'
    
    def authorize(username, password)
      request = Payleven::Request.new(PATH_AUTHORIZE, 'POST')
      params = {"username" => username, "password" => password}
      puts request.parameters
      request.parameters.merge!(params)
      response = request.run
      response
    end

  end
end