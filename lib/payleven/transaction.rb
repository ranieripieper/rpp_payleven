# encoding: utf-8
require 'openssl'
require 'base64'
require File.join(File.dirname(__FILE__), '..', 'payleven')

module Payleven
  class Transaction < BaseService
    
    URL_GET_TRANSACTIONS = '/partner/transactions'
    URL_GENERATE_TOKEN = '/partner/createToken'
    URL_PROCESS_WITH_TOKEN = '/partner/processWithToken'
    URL_AUTHORIZE_WITH_TOKEN = '/partner/authorizeWithToken'
    URL_CAPTURE_WITH_TID = '/partner/captureWithTid'
    URL_SEND_RECEIPT = '/partner/send-receipt'
    URL_REFUND = '/partner/refund_request'
    
    def get_transactions(data)
      request = Payleven::Request.new(URL_GET_TRANSACTIONS, 'GET', Payleven.endpoint_minha_conta)
      params = {
                  "entity" => data, 
                  "access" => Payleven.access_key, 
                  "token" => signature_token(data, Payleven.secret_key)}

      request.parameters.merge!(params)
      
      response = request.run
      response
    end
     
    def create_token(cc_number, cc_holder, cc_brand, cc_cvv, cc_expiration, access_token)
      request = Payleven::Request.new(URL_GENERATE_TOKEN, 'POST', Payleven.endpoint_gateway)
      params = {
                  "cc_number" => cc_number, 
                  "cc_holder" => cc_holder, 
                  "cc_brand" => cc_brand, 
                  "cc_cvv" => cc_cvv, 
                  "cc_expiration" => cc_expiration}

      request.parameters.merge!(params)
      request.headers.merge!({"x-partner-token" => access_token})
      
      response = request.run
      response
    end
    
    def process_with_token(value, payleven_id, request_id, card_holder_reference, cc_brand, cc_token, access_token)
      request = Payleven::Request.new(URL_PROCESS_WITH_TOKEN, 'POST', Payleven.endpoint_gateway)
      params = {
                  "value" => value, 
                  "payleven_id" => payleven_id, 
                  "card_holder_reference" => card_holder_reference, 
                  "request_id" => request_id, 
                  "cc_brand" => cc_brand,
                  "token" => cc_token}

      request.parameters.merge!(params)
      
      request.headers.merge!({"x-partner-token" => access_token})
      
      response = request.run
      response
    end
    
    def authorize_with_token(value, payleven_id, request_id, card_holder_reference, cc_brand, cc_token, access_token)
      request = Payleven::Request.new(URL_AUTHORIZE_WITH_TOKEN, 'POST', Payleven.endpoint_gateway)
      params = {
                  "value" => value, 
                  "payleven_id" => payleven_id, 
                  "card_holder_reference" => card_holder_reference, 
                  "request_id" => request_id, 
                  "cc_brand" => cc_brand,
                  "token" => cc_token}

      request.parameters.merge!(params)
      
      request.headers.merge!({"x-partner-token" => access_token})
      
      response = request.run
      response
    end
    
    
    def capture_with_tid(request_id, value, transaction_id, access_token)
      request = Payleven::Request.new(URL_CAPTURE_WITH_TID, 'POST', Payleven.endpoint_gateway)
      params = {
                  "value" => value, 
                  "tid" => transaction_id,
                  "request_id" => request_id}

      request.parameters.merge!(params)
      
      request.headers.merge!({"x-partner-token" => access_token})
      
      response = request.run
      response
    end
    
    def refund(transaction_id)
      
      data = {
        "transaction_id" => transaction_id,
        'timestamp' => Payleven::BaseService.get_timestamp
      }
      request = Payleven::Request.new(URL_REFUND, 'GET', Payleven.endpoint_minha_conta)
      params = {
                  "entity" => data,
                  "access" => Payleven.access_key, 
                  "token" => signature_token(data, Payleven.secret_key)}

      request.query.merge!(params)
      
      response = request.run
      response
    end
    
    def send_receipt(transaction_id, email, phone, access_token)
      request = Payleven::Request.new(URL_SEND_RECEIPT, 'GET', Payleven.endpoint_gateway)
      params = {
                  "email" => email, 
                  "tid" => transaction_id,
                  "phone" => phone}

      request.query.merge!(params)
      
      request.headers.merge!({"x-partner-token" => access_token})
      
      response = request.run
      response
    end
    
  end
end