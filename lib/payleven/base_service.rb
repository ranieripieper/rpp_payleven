# encoding: utf-8
require 'openssl'
require 'base64'
require File.join(File.dirname(__FILE__), '..', 'payleven')

module Payleven
  
  class BaseService
    def signature_token(data, secret_key)
      hash = {}
      if data["entity"].nil? then
        hash = data
      else
        hash = data["entity"]
      end

      data_sginature = hash.to_s.gsub("=>", ":").gsub(", \"", ",").gsub("\"", "").gsub("{", "").gsub("}", "")
      digest_sha512 = OpenSSL::Digest.new('sha512')
      signature_hmac = OpenSSL::HMAC.hexdigest(digest_sha512, secret_key, data_sginature)
      signature = Digest::SHA1.hexdigest(signature_hmac)
      signature
    end
    
    
    def self.get_timestamp
      DateTime.now.strftime('%Y-%m-%d %H:%M:%S')
    end

  end
end