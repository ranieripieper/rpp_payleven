# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'payleven/version'

Gem::Specification.new do |spec|
  spec.name          = "payleven"
  spec.version       = Payleven::VERSION
  spec.authors       = ["Rani Pieper"]
  spec.email         = ["ranieripieper@gmail.com"]

  spec.summary       = "Integração payleven"
  spec.description   = "Integração payleven"
  spec.homepage      = "http://www.doisdoissete.com"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency('shoulda', '~> 3.4.0')
  spec.add_development_dependency('test-unit')

  spec.add_dependency "rest-client"
  spec.add_dependency "multi_json"
  spec.add_dependency "rack"
  
end
